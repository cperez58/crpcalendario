//
//  main.m
//  CRPCalendario
//
//  Created by CARLOS RAUL PEREZ MORENO on 16/07/14.
//
//

#import <UIKit/UIKit.h>

#import "CRPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CRPAppDelegate class]));
    }
}
