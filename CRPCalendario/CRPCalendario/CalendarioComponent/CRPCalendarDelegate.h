//
//  CRPCalendarDelegate.h
//  CRPCalendar
//
//  Created by Carlos Raúl Pérez Moreno on 17/03/14.
//  
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CRPCalendarAccessMode) {
    CRPCalendarUndefinedMode = 0,
    CRPCalendarDefaultMode = 1,
    CRPCalendarFromTodayMode = 2,
    CRPCalendarTillTodayMode = 3,
    CRPCalendarTillTodayModeWithButton = 4,
    CRPCalendarTillTodayPeriodMode = 5,
    CRPCalendarFromTodayModeWithButton = 6
};

@protocol CRPCalendarDelegate <NSObject>

@optional
- (void)onTapElementFromCalendar:(id)selectedDate;
- (void)onPanPeriodSelected:(NSArray *)periodArray;
- (void)onTapChangeMonth:(NSDictionary *)dictMonthSelected;
- (void)onTapMonthButton:(NSDictionary *)dictMonthSelected;

@end
