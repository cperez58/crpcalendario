//
//  CRPContainer.h
//  CRPCalendario
//
//  Created by CARLOS RAUL PEREZ MORENO on 16/07/14.
//
//

#import "CRPContainer.h"

@implementation CRPContainer

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configureView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configureView];
    }
    
    return self;
}

- (void)setTag:(NSInteger)tag {
    [super setTag:tag];
    [self configureView];
}

- (void)configureView {
    switch (self.tag) {
        case TAG_CONTAINER_MAIN:
            self.layer.borderColor = COLOR_BORDER_CONTAINER.CGColor;
            self.layer.borderWidth = SIZE_CONTAINER_BORDER;
            break;
    }
}

@end
