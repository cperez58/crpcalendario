//
//  CRPCalendarComponent.h
//  CRPCalendar
//
//  Created by Carlos Raúl Pérez Moreno on 24/02/14.
//  
//

#import <UIKit/UIKit.h>
#import "CRPContainer.h"
#import "CRPCalendarDelegate.h"

#define kMonth @"month"
#define kYear @"year"
#define kPayments @"payments"
#define kDay @"day"
#define kPayment @"payment"
#define kAmount @"amount"

#define kInitialWeekDay @"initialWeekDay"
#define kNumDaysMonth @"numDaysMonth"

#define kCollectionCellIdentifierCalendarElement @"calendarItemCell"
#define kTimeAnimationCalendarAppears 0.25
#define kCalendarComponentSize CGSizeMake(320, 240)

#define kNumYearsAtComponent 100
#define kNumElementsAtCalendarRow 7
#define kNumElementsAtCalendarColumn 6
#define kHeightCalendarCell 30
#define kWidthCalendarCell 37

@interface CRPCalendarComponent : UIView

@property (weak, nonatomic) IBOutlet UICollectionView *collectionCalendarView;
@property (weak, nonatomic) IBOutlet UILabel *lblMonthYear;
@property (weak, nonatomic) IBOutlet UIButton *btnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) NSDate *dateSelected;
@property (nonatomic) BOOL isCalendarOnView;
@property (weak, nonatomic) IBOutlet CRPContainer *containerLeft;
@property (weak, nonatomic) IBOutlet CRPContainer *containerRight;
@property (nonatomic) CRPCalendarAccessMode accessMode;
@property (nonatomic, weak) id<CRPCalendarDelegate> delegate;
@property (nonatomic, strong) NSArray *arrayEventsData;
@property (weak, nonatomic) IBOutlet UIView *viewMargin;
@property (nonatomic, strong) NSIndexPath *indexPathCurrent;
@property (nonatomic, strong) NSDate *dateIteratorFirstDayMonth;
@property (nonatomic, strong) NSIndexPath *indexPathDateSelected;
@property (nonatomic, strong) NSIndexPath *indexStartPeriod;
@property (nonatomic, strong) NSIndexPath *indexEndPeriod;
@property (nonatomic, strong) NSMutableSet * eventsScheduled;
@property (nonatomic, strong) NSIndexPath *indexHighlightedPrev;
@property (nonatomic) BOOL onStartPeriod;

-(id)initWithFrame:(CGRect)frame withAccessMode:(CRPCalendarAccessMode)mode;
-(void)selectCurrentElement;
-(void)resetCalendar;
-(void)resetCalendarGoingToday;
-(void)resetCalendarGoingSelectedDate;
-(void)configureDefaultAccessMode;
-(void)configureFromTodayAccessMode:(BOOL)enable;
-(void)configureTillTodayAccessMode:(BOOL)enable;
-(NSIndexPath *)indexFromDate:(NSDate *)dateSelected;
-(void)completePeriodBetween:(NSIndexPath *)indexStart and:(NSIndexPath *)indexEnd;
-(NSDate *)dateFromSelection:(NSIndexPath *)indexPath;
-(NSString *)fromDateToLiteralTitle:(NSDate *)dateSelected;
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath_;
@end
