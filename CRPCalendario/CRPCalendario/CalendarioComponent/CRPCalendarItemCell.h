//
//  CRPCalendarItemCell.h
//  CRPCalendar
//
//  Created by Carlos Raúl Pérez Moreno on 24/02/14.
//  
//

#import <UIKit/UIKit.h>
#import "CRPContainer.h"

#define kRolElementPeriodStarter @"kRolElementPeriodStarter"
#define kRolElementPeriodStarterDisabled @"kRolElementPeriodStarterDisabled"
#define kRolElementPeriodCentral @"kRolElementPeriodCentral"
#define kRolElementPeriodEnder @"kRolElementPeriodEnder"
#define kRolElementPeriodEnderDisabled @"kRolElementPeriodEnderDisabled"
#define kRolElementEventsScheduled @"kRolElementEventsScheduled"
#define kRolElementDefault @"kRolElementDefault"
#define kRolElementDefaultPeriod @"kRolElementDefaultPeriod"

@interface CRPCalendarItemCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDayNumber;
@property (weak, nonatomic) IBOutlet UIView *viewEventsIndicator;
@property (weak, nonatomic) IBOutlet UIView *viewEndPeriodIndicator;
@property (weak, nonatomic) IBOutlet UIView *viewEndPeriodIndicatorBottom;
@property (weak, nonatomic) IBOutlet UIView *viewEndPeriodIndicatorTop;
@property (weak, nonatomic) IBOutlet UIView *viewEndPeriodIndicatorLeft;
@property (weak, nonatomic) IBOutlet UIView *viewStartPeriodIndicator;
@property (weak, nonatomic) IBOutlet UIView *viewStartPeriodIndicatorTop;
@property (weak, nonatomic) IBOutlet UIView *viewStartPeriodIndicatorBottom;
@property (weak, nonatomic) IBOutlet UIView *viewStartPeriodIndicatorRight;
@property (weak, nonatomic) IBOutlet UIView *todayIndicator;
@property (weak, nonatomic) IBOutlet CRPContainer *viewContainer;

@property (nonatomic) BOOL enabledDay;
@property (nonatomic) UICollectionViewScrollDirection direction;
@property (nonatomic) NSString *rolCell;

- (void)configureViewWithData:(NSDictionary *)dictData withIndex:(NSIndexPath *)indexPath;
//- (void)setElementAs:(NSString *)rolElement;
- (void)setTodayCell:(BOOL)isToday;

@end
