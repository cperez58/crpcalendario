//
//  CRPCalendarItemCell.m
//  CRPCalendar
//
//  Created by Carlos Raúl Pérez Moreno on 24/02/14.
//  
//

#import "CRPCalendarItemCell.h"
#import <QuartzCore/QuartzCore.h>

@interface CRPCalendarItemCell(){

}

@property (nonatomic) BOOL couldBeActive;
@property (nonatomic) BOOL partOfAPeriod;
@property (nonatomic, strong) NSIndexPath *indexPathCell;
@property (nonatomic, strong) NSDictionary *dictData;
@property (nonatomic) BOOL isToday;


@end

@implementation CRPCalendarItemCell


- (id)init
{
    self = [super init];
    if (self) {
        [self configureUI];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configureUI];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configureUI];
    }
    return self;
}

- (void)configureUI{
    _couldBeActive = YES;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self.viewContainer setBackgroundColor:COLOR_VIEW_CONTROLLER_BACKGROUND];
    
    UIBezierPath *leftBorder = [UIBezierPath bezierPathWithRoundedRect:self.todayIndicator.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerBottomLeft|UIRectCornerTopRight|UIRectCornerBottomRight cornerRadii:CGSizeMake(self.bounds.size.height / 2, self.todayIndicator.bounds.size.height / 2)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:leftBorder.CGPath];
    self.todayIndicator.layer.mask = shape;
    self.todayIndicator.layer.shouldRasterize = YES;
}

- (void)configureViewWithData:(NSDictionary *)dictData withIndex:(NSIndexPath *)indexPath{
    NSNumber *numEmptyDays = @0;
    NSNumber *numMonthDays = @0;
    if (dictData) {
        numEmptyDays = [dictData objectForKey:@"initialWeekDay"];
        numMonthDays = [dictData objectForKey:@"numDaysMonth"];
    }
    _dictData = dictData;
    NSIndexPath *indexTransform = [NSIndexPath indexPathForRow:indexPath.row /6 + (indexPath.row%6 *7) inSection:indexPath.section];
    if (_direction == UICollectionViewScrollDirectionHorizontal && indexTransform.row>=[numEmptyDays integerValue] && indexTransform.row < [numMonthDays integerValue]+[numEmptyDays integerValue]) {
        [self.lblDayNumber setText:[NSString stringWithFormat:@"%ld", indexTransform.row + 1L - [numEmptyDays integerValue]]];
        _couldBeActive = YES;
    }else if (indexPath.row>=[numEmptyDays integerValue] && indexPath.row < [numMonthDays integerValue]+[numEmptyDays integerValue]) {
        [self.lblDayNumber setText:[NSString stringWithFormat:@"%ld", indexPath.row + 1L - [numEmptyDays integerValue]]];
        _couldBeActive = YES;
    }else{
        [self.lblDayNumber setText:@""];
        _couldBeActive = NO;
    }
    _indexPathCell = indexPath;
    [self.viewEndPeriodIndicator setHidden:YES];
    [self.viewEndPeriodIndicatorBottom setHidden:YES];
    [self.viewEndPeriodIndicatorTop setHidden:YES];
    [self.viewEndPeriodIndicatorLeft setHidden:YES];
    
    [self.viewStartPeriodIndicator setHidden:YES];
    [self.viewStartPeriodIndicatorBottom setHidden:YES];
    [self.viewStartPeriodIndicatorTop setHidden:YES];
    [self.viewStartPeriodIndicatorRight setHidden:YES];
    
    [self setRolCell:kRolElementDefault];
}

- (void)setSelected:(BOOL)selected{
    
    NSNumber *numEmptyDays = @0;
    NSNumber *numMonthDays = @0;
    if (_dictData) {
        numEmptyDays = [_dictData objectForKey:@"initialWeekDay"];
        numMonthDays = [_dictData objectForKey:@"numDaysMonth"];
    }
    if (_indexPathCell.row>=[numEmptyDays integerValue] && _indexPathCell.row < [numMonthDays integerValue]+[numEmptyDays integerValue] && self.enabledDay) {
        if (selected) {
            [self.viewContainer setBackgroundColor:COLOR_CALENDAR_SELECTED_DAY];
            [self.lblDayNumber setTextColor:COLOR_CALENDAR_TEXT_SELECTED_DAY];
            if (_isToday) {
                [self.todayIndicator setBackgroundColor:COLOR_CALENDAR_TEXT_SELECTED_DAY];
            }
        }else if (!selected){
            if (!_rolCell || [_rolCell isEqualToString:@""] || [_rolCell isEqualToString:kRolElementDefault]){
                [self.viewContainer setBackgroundColor: COLOR_VIEW_CONTROLLER_BACKGROUND];
            }
            [self.lblDayNumber setTextColor: COLOR_LABEL_BLACK];
            [self setRolCell:_rolCell];
            if (_isToday) {
                [self.todayIndicator setBackgroundColor:COLOR_CALENDAR_SELECTED_DAY];
            }
        }
    }else{
        if (!_rolCell || [_rolCell isEqualToString:@""] || [_rolCell isEqualToString:kRolElementDefault]){
            [self.viewContainer setBackgroundColor: COLOR_VIEW_CONTROLLER_BACKGROUND];
        }
        [self.lblDayNumber setTextColor: COLOR_LABEL_THIRD];
        [self setRolCell:_rolCell];
    }
    
    if ([_rolCell isEqualToString:kRolElementPeriodStarter] || [_rolCell isEqualToString:kRolElementPeriodEnder] || [_rolCell isEqualToString:kRolElementPeriodCentral] || [_rolCell isEqualToString:kRolElementPeriodStarterDisabled] || [_rolCell isEqualToString:kRolElementPeriodEnderDisabled]) {
        [self.viewContainer setBackgroundColor: COLOR_VIEW_CONTROLLER_BACKGROUND];
        [self.lblDayNumber setTextColor: COLOR_LABEL_BLACK];
        [self setRolCell:_rolCell];
    }
}

- (void)setHighlighted:(BOOL)highlighted{
    if (highlighted) {
        [self.viewContainer setBackgroundColor:COLOR_CALENDAR_TEXT_SELECTED_DAY];
    }else{
        [self.viewContainer setBackgroundColor:COLOR_VIEW_CONTROLLER_BACKGROUND];
    }
}

- (void)setTodayCell:(BOOL)isToday{
    _isToday = isToday;
    [self.todayIndicator setHidden:!isToday];
}

- (void)setPartOfAPeriod:(BOOL)partOfAPeriod{
    _partOfAPeriod = partOfAPeriod;
}

- (void)setEnabledDay:(BOOL)enabled{
    _enabledDay = enabled;
    if (enabled) {
        [self.lblDayNumber setTextColor: COLOR_LABEL_BLACK];
        _couldBeActive = YES;
    }else{
        [self.lblDayNumber setTextColor: COLOR_LABEL_THIRD];
        _couldBeActive = NO;
    }
}

- (void)setRolCell:(NSString *)rolCell{
    _rolCell = rolCell;
    if ([rolCell isEqualToString:kRolElementPeriodStarter] || [rolCell isEqualToString:kRolElementPeriodStarterDisabled]) {
        //Get end period indicator colored
        [self setColorEndPeriodIndicator:[UIColor clearColor]];
        [self setHiddenEndPeriodIndicator:YES];
        //Get start period indicator colored
        [self setColorStartPeriodIndicator:([rolCell isEqualToString:kRolElementPeriodStarter])?COLOR_BORDER_CONTAINER_SELECTED:COLOR_BORDER_CONTAINER_DETAILS];
        [self setHiddenStartPeriodIndicator:NO];
        
        [self.viewContainer setBackgroundColor:COLOR_CALENDAR_TEXT_SELECTED_DAY];
        [self.viewEventsIndicator setHidden:YES];
    }else if ([rolCell isEqualToString:kRolElementPeriodEnder] || [rolCell isEqualToString:kRolElementPeriodEnderDisabled]){
        //Get end period indicator colored
        [self setColorEndPeriodIndicator:([rolCell isEqualToString:kRolElementPeriodEnder])?COLOR_BORDER_CONTAINER_SELECTED:COLOR_BORDER_CONTAINER_DETAILS];
        [self setHiddenEndPeriodIndicator:NO];
        //Get start period indicator colored
        [self setColorStartPeriodIndicator:[UIColor clearColor]];
        [self setHiddenStartPeriodIndicator:YES];
        
        [self.viewContainer setBackgroundColor:COLOR_CALENDAR_TEXT_SELECTED_DAY];
        [self.viewEventsIndicator setHidden:YES];
    }else if ([rolCell isEqualToString:kRolElementPeriodCentral]){
        //Get end period indicator colored
        [self setColorEndPeriodIndicator:[UIColor clearColor]];
        [self setHiddenEndPeriodIndicator:YES];
        //Get start period indicator colored
        [self setColorStartPeriodIndicator:[UIColor clearColor]];
        [self setHiddenStartPeriodIndicator:YES];
        
        [self.viewContainer setBackgroundColor:COLOR_CALENDAR_TEXT_SELECTED_DAY];
        [self.viewEventsIndicator setHidden:YES];
    }else if ([rolCell isEqualToString:kRolElementEventsScheduled]){
        [self setHiddenEndPeriodIndicator:YES];
        [self setHiddenStartPeriodIndicator:YES];
        
        [self.viewContainer setBackgroundColor:COLOR_VIEW_CONTROLLER_BACKGROUND];
        [self.viewEventsIndicator setHidden:NO];
        [self.viewEventsIndicator setBackgroundColor:COLOR_CALENDAR_SELECTED_DAY];
        
    }else if (!rolCell || [rolCell isEqualToString:@""] || [rolCell isEqualToString:kRolElementDefault]){
        [self.viewContainer setBackgroundColor:COLOR_VIEW_CONTROLLER_BACKGROUND];
        [self setHiddenEndPeriodIndicator:YES];
        [self setHiddenStartPeriodIndicator:YES];
        
        [self.viewEventsIndicator setHidden:YES];
    }
}

- (void)setColorStartPeriodIndicator:(UIColor *)color{
    [self.viewStartPeriodIndicator setBackgroundColor:color];
    [self.viewStartPeriodIndicatorBottom setBackgroundColor:color];
    [self.viewStartPeriodIndicatorTop setBackgroundColor:color];
    [self.viewStartPeriodIndicatorRight setBackgroundColor:color];
}

- (void)setHiddenStartPeriodIndicator:(BOOL)hide{
    [self.viewStartPeriodIndicator setHidden:hide];
    [self.viewStartPeriodIndicatorBottom setHidden:hide];
    [self.viewStartPeriodIndicatorTop setHidden:hide];
    [self.viewStartPeriodIndicatorRight setHidden:hide];
}

- (void)setColorEndPeriodIndicator:(UIColor *)color{
    [self.viewEndPeriodIndicator setBackgroundColor:color];
    [self.viewEndPeriodIndicatorBottom setBackgroundColor:color];
    [self.viewEndPeriodIndicatorTop setBackgroundColor:color];
    [self.viewEndPeriodIndicatorLeft setBackgroundColor:color];
}

- (void)setHiddenEndPeriodIndicator:(BOOL)hide{
    [self.viewEndPeriodIndicator setHidden:hide];
    [self.viewEndPeriodIndicatorBottom setHidden:hide];
    [self.viewEndPeriodIndicatorTop setHidden:hide];
    [self.viewEndPeriodIndicatorLeft setHidden:hide];
}

@end
