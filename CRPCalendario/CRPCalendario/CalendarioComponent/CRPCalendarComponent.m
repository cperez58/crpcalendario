//
//  CRPCalendarComponent.m
//  CRPCalendar
//
//  Created by Carlos Raúl Pérez Moreno on 24/02/14.
//  
//

#import "CRPCalendarComponent.h"
#import "CRPCalendarItemCell.h"

@interface CRPCalendarComponent() <UIGestureRecognizerDelegate>{

}

@property (nonatomic, strong) NSDictionary *dictCalendarItemPrev;
@property (nonatomic) long sectionPrev;

@end

@implementation CRPCalendarComponent

- (id)init
{
    self = [super init];
    if (self) {
        [self configureView];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame withAccessMode:(CRPCalendarAccessMode)mode{
    self = [super initWithFrame:frame];
    if (self) {
        _isCalendarOnView = NO;
        _accessMode = mode;
        [self configureView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configureView];
        _isCalendarOnView = NO;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configureView];
    }
    return self;
}

- (void)configureView {
    
    UIView *root = [[[NSBundle mainBundle] loadNibNamed:@"CRPCalendarComponent" owner:self options:nil] lastObject];
    [self addSubview:root];
    [self.collectionCalendarView registerNib:[UINib nibWithNibName:@"CRPCalendarItemCell" bundle:nil] forCellWithReuseIdentifier:kCollectionCellIdentifierCalendarElement];
    //If delegate implements onPanPeriodSelected, Calendar adds PanGestureRecognizer
    if ([self.delegate respondsToSelector:@selector(onPanPeriodSelected:)]) {
        [self addPanGestureToView:self.collectionCalendarView];
    }
    [self.viewMargin setBackgroundColor:COLOR_BORDER_CONTAINER];
    
    [self.containerRight setBackgroundColor:COLOR_VIEW_CONTROLLER_BACKGROUND];
    [self.containerLeft setBackgroundColor:COLOR_VIEW_CONTROLLER_BACKGROUND];

    [self configureDefaultAccessMode];
    if (self.accessMode == CRPCalendarFromTodayMode || self.accessMode == CRPCalendarFromTodayModeWithButton) {
        [self configureFromTodayAccessMode:NO];
    }
}

#pragma mark - gestureRecognizers

- (void)addPanGestureToView:(UIView *)view {
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    panGesture.delegate = self;
    panGesture.maximumNumberOfTouches = 1;
    panGesture.minimumNumberOfTouches = 1;
    [view addGestureRecognizer:panGesture];
}

- (void)handlePan:(UIGestureRecognizer *)sender {
	
    if ([sender isKindOfClass:[UIPanGestureRecognizer class]]) {
        UIPanGestureRecognizer *pan = (UIPanGestureRecognizer *)sender;
        
        if (pan.state == UIGestureRecognizerStateBegan) {
            CGPoint locationBeforePan = [pan locationInView:self.collectionCalendarView];
            NSIndexPath *indexPathConverted = [self indexPathConversionDependsOnScrollOrientation:[self indePathFromPoint:locationBeforePan] inverse:YES];
            _indexStartPeriod = indexPathConverted;
            _indexHighlightedPrev = _indexStartPeriod;
        }
        
        if (pan.state == UIGestureRecognizerStateChanged) {
            CGPoint locationBeforePan = [pan locationInView:self.collectionCalendarView];
            NSIndexPath *prevIndexTap = nil;
            NSIndexPath *nextIndexTap = nil;
            
            if([_indexHighlightedPrev compare:nextIndexTap] < 0){
                prevIndexTap = _indexHighlightedPrev;
                NSIndexPath *indexPathConverted = [self indexPathConversionDependsOnScrollOrientation:[self indePathFromPoint:locationBeforePan] inverse:YES];
                nextIndexTap = indexPathConverted;
                
                for (long j = prevIndexTap.section; j < nextIndexTap.section+1L ; j++) {
                    for (long i = prevIndexTap.row; i < nextIndexTap.row+1L; i++) {
                        CRPCalendarItemCell *cell = (CRPCalendarItemCell *)[self.collectionCalendarView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:j]];
                        [cell setHighlighted:!cell.highlighted];
                    }
                }
            }else{
                NSIndexPath *indexPathConverted = [self indexPathConversionDependsOnScrollOrientation:[self indePathFromPoint:locationBeforePan] inverse:YES];
                prevIndexTap = indexPathConverted;
                nextIndexTap = _indexHighlightedPrev;
                
                for (long j = prevIndexTap.section; j < nextIndexTap.section+1L; j++) {
                    for (long i = prevIndexTap.row; i < nextIndexTap.row+1L; i++) {
                        CRPCalendarItemCell *cell = (CRPCalendarItemCell *)[self.collectionCalendarView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:j]];
                        [cell setHighlighted:!cell.highlighted];
                    }
                }
            }
            
            _indexHighlightedPrev = nextIndexTap;
        }
        
        if (sender.state == UIGestureRecognizerStateEnded) {
            CGPoint locationAfterPan = [pan locationInView:self.collectionCalendarView];
            if (locationAfterPan.x >0 && locationAfterPan.y>0) {
                NSIndexPath *indexPathConverted = [self indexPathConversionDependsOnScrollOrientation:[self indePathFromPoint:locationAfterPan] inverse:YES];
                _indexEndPeriod = indexPathConverted;
                [self completePeriodBetween:_indexStartPeriod and:_indexEndPeriod];
            }else{
                [self undoPan];
            }
        } else if (sender.state == UIGestureRecognizerStateCancelled) {
            [self undoPan];
        }
    }
}

- (void)completePeriodBetween:(NSIndexPath *)indexStart and:(NSIndexPath *)indexEnd {
    NSIndexPath *minorIndexPath = nil;
    NSIndexPath *highIndexPath = nil;
    
    if ([indexStart compare:indexEnd] < 0) {
        minorIndexPath = indexStart;
        highIndexPath = indexEnd;
    }else if ([indexStart compare:indexEnd] > 0){
        minorIndexPath = indexEnd;
        highIndexPath = indexStart;
    }
    
    for (long j = minorIndexPath.section; j < highIndexPath.section+1L; j++){
        for (long i = 0; i < kNumElementsAtCalendarRow * kNumElementsAtCalendarColumn; i++) {
            
            BOOL isMinorIndexElement = (i == minorIndexPath.row && j == minorIndexPath.section);
            BOOL isHighIndexElement = (i == highIndexPath.row && j == highIndexPath.section);
            BOOL isAIntermediateSection = (highIndexPath.section != minorIndexPath.section && j < highIndexPath.section && j > minorIndexPath.section);
            BOOL isLastSectionUnderLastRow = (highIndexPath.section != minorIndexPath.section && i < highIndexPath.row && j == highIndexPath.section);
            BOOL isFirstSectionUpFirstRow = (highIndexPath.section != minorIndexPath.section && j == minorIndexPath.section && i > minorIndexPath.row);
            
            BOOL isSameSectionDifferentRow = (highIndexPath.section == minorIndexPath.section && i < highIndexPath.row && i > minorIndexPath.row);
            
            CRPCalendarItemCell *cell = (CRPCalendarItemCell *)[self.collectionCalendarView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:j]];
            if (isMinorIndexElement) {
                [cell setRolCell:(_onStartPeriod)?kRolElementPeriodStarterDisabled:kRolElementPeriodStarter];
            }else if (isHighIndexElement){
                [cell setRolCell:(_onStartPeriod)?kRolElementPeriodEnder:kRolElementPeriodEnderDisabled];
            }else if (isFirstSectionUpFirstRow || isLastSectionUnderLastRow || isAIntermediateSection || isSameSectionDifferentRow){
                [cell setRolCell:kRolElementPeriodCentral];
            }else{
                [cell setRolCell:kRolElementDefault];
            }
        }
    }
}

- (void)undoPan {
    
}

#pragma mark - UICollectionViewDelegate & UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 12*kNumYearsAtComponent;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return kNumElementsAtCalendarColumn * kNumElementsAtCalendarRow;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath_{
    
    NSIndexPath *indexPath = [self indexPathConversionDependsOnScrollOrientation:indexPath_ inverse:NO];
    
    CRPCalendarItemCell *cell = (CRPCalendarItemCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCollectionCellIdentifierCalendarElement forIndexPath:indexPath];
    
    NSDictionary *dictCalendarItem;
    if (_sectionPrev == indexPath_.section) {
        dictCalendarItem = _dictCalendarItemPrev;
    }else{
        dictCalendarItem = [self configurationParamsForIndex:indexPath];
    }
    
    cell.direction = [((UICollectionViewFlowLayout *)[collectionView collectionViewLayout]) scrollDirection];
    [cell configureViewWithData:dictCalendarItem withIndex:indexPath];
    
    NSIndexPath *indexPathToday = [self indexPathConversionDependsOnScrollOrientation:[self indexFromDate:[NSDate date]] inverse:NO];
    if ([indexPath compare:indexPathToday] == 0) {
        [cell setTodayCell:YES];
    }else{
        [cell setTodayCell:NO];
    }
    //Access Mode From Today disable all dates previous today.
    //Access Mode Till Today disable all dates from today.
    if (self.accessMode == CRPCalendarFromTodayMode) {
        
        if ([indexPath compare:indexPathToday] <0) {
            [cell setEnabledDay:NO];
        }else if ([indexPath compare:indexPathToday] >= 0){
            [cell setEnabledDay:YES];
        }
        if ((_indexPathDateSelected && [indexPath compare:_indexPathDateSelected]==0) || (!_indexPathDateSelected && [indexPath compare:indexPathToday]==0)) {
            [cell setSelected:YES];
        }
    }else if (self.accessMode == CRPCalendarFromTodayModeWithButton) {
        [cell setTodayCell:([indexPath compare:indexPathToday] == 0)];
        if ([self.eventsScheduled containsObject:indexPath]) {
            [cell setRolCell:kRolElementEventsScheduled];
            [cell setEnabledDay:YES];
        }else{
            [cell setEnabledDay:NO];
        }
    }else if (self.accessMode == CRPCalendarTillTodayMode || self.accessMode == CRPCalendarTillTodayModeWithButton || self.accessMode == CRPCalendarTillTodayPeriodMode){
        
        if (self.accessMode == CRPCalendarTillTodayPeriodMode) {
            if (([indexPath compare:_indexStartPeriod] < 0 && [_indexHighlightedPrev isEqual:_indexStartPeriod])||([indexPath compare:indexPathToday] > 0)) {
                [cell setEnabledDay:NO];
            }else if (([indexPath compare:_indexEndPeriod] > 0 && [_indexHighlightedPrev isEqual:_indexEndPeriod])||([indexPath compare:indexPathToday] > 0)){
                [cell setEnabledDay:NO];
            }else if ([indexPath compare:indexPathToday] <= 0){
                [cell setEnabledDay:YES];
            }
            
            [cell setTodayCell:([indexPath compare:indexPathToday] == 0)];
            
            if (_indexEndPeriod && [indexPath_ isEqual:_indexEndPeriod]) {
                [cell setRolCell:(_onStartPeriod)?kRolElementPeriodEnder:kRolElementPeriodEnderDisabled];
            }
            if (_indexStartPeriod && [indexPath_ isEqual:_indexStartPeriod]) {
                [cell setRolCell:(_onStartPeriod)?kRolElementPeriodStarterDisabled:kRolElementPeriodStarter];
            }
            
            BOOL isAIntermediateAtSameSection = (_indexEndPeriod.section == _indexStartPeriod.section && indexPath_.section ==_indexEndPeriod.section && indexPath_.row > _indexStartPeriod.row && indexPath_.row<_indexEndPeriod.row);
            BOOL isLastSectionUnderLastRow = (_indexEndPeriod.section != _indexStartPeriod.section && indexPath_.row < _indexEndPeriod.row && indexPath_.section == _indexEndPeriod.section);
            BOOL isFirstSectionUpFirstRow = (_indexEndPeriod.section != _indexStartPeriod.section && indexPath_.section == _indexStartPeriod.section && indexPath_.row > _indexStartPeriod.row);
            BOOL isAIntermediateAtDifferentSection = (_indexEndPeriod.section != _indexStartPeriod.section && ((_indexEndPeriod.section == indexPath_.section && indexPath_.row < _indexEndPeriod.row) || (_indexStartPeriod.section == indexPath_.section && indexPath_.row > _indexStartPeriod.row)|| (_indexEndPeriod.section > indexPath_.section && _indexStartPeriod.section < indexPath_.section)));
            
            if ((isAIntermediateAtSameSection || isLastSectionUnderLastRow || isFirstSectionUpFirstRow || isAIntermediateAtDifferentSection) && _indexStartPeriod && _indexEndPeriod && [cell enabledDay]) {
                [cell setRolCell:kRolElementPeriodCentral];
            }
        }else{
            if ([indexPath compare:indexPathToday] <= 0) {
                [cell setEnabledDay:YES];
            }else if ([indexPath compare:indexPathToday] > 0){
                [cell setEnabledDay:NO];
            }
        }
    }else if ([indexPath compare:indexPathToday] != 0) {
        [cell setEnabledDay:YES];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath_{
   
    NSDate *dateSelected;
    
    NSIndexPath *indexPath = [self indexPathConversionDependsOnScrollOrientation:indexPath_ inverse:YES];
    
    dateSelected = [self dateFromSelection:indexPath];
    
    BOOL isIndexUpLimit = indexPath.row >= [[self startWeekdayForMonthOfDate:_dateIteratorFirstDayMonth] integerValue];
    BOOL isIndexDownLimit = indexPath.row < [[self startWeekdayForMonthOfDate:_dateIteratorFirstDayMonth] integerValue]+ [[self numberOfDaysAtMonthForDate:_dateIteratorFirstDayMonth] integerValue];
    
    NSDictionary *dictCalendarItem;
    dictCalendarItem = [self configurationParamsForIndex:indexPath];
    NSNumber *numEmptyDays = [dictCalendarItem objectForKey:@"initialWeekDay"];
    NSNumber *numMonthDays = [dictCalendarItem objectForKey:@"numDaysMonth"];
    BOOL isAValidDayCell = (indexPath_.row>=[numEmptyDays integerValue] && indexPath_.row < [numMonthDays integerValue]+[numEmptyDays integerValue]);
    
    CRPCalendarItemCell *cell = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexPath_];
    [cell setClipsToBounds:NO];
    if ([cell enabledDay] && isAValidDayCell) {
        if (self.accessMode == CRPCalendarTillTodayPeriodMode && !_onStartPeriod) {
            
            if (_indexHighlightedPrev) {
                for (long j = 0; j < labs(_indexStartPeriod.section - indexPath_.section)+1L; j++) {
                    for(long i=0; i < labs(_indexStartPeriod.row - indexPath_.row)+1L; i++){
                        long originRow = 0L;
                        long originSection = 0L;
                        if (indexPath_.section == _indexStartPeriod.section){
                            originSection = indexPath_.section;
                            if (indexPath_.row < _indexStartPeriod.row) {
                                originRow = indexPath_.row;
                                NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:originRow + i inSection:originSection + j];
                                CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                                [cellPrev setRolCell:kRolElementPeriodCentral];
                            }else{
                                originRow = _indexStartPeriod.row;
                                NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:originRow + i inSection:indexPath_.section];
                                CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                                [cellPrev setRolCell:kRolElementDefault];
                            }
                        }else if (indexPath_.section > _indexStartPeriod.section){
                            originSection = _indexStartPeriod.section;
                            if (originSection + j == _indexStartPeriod.section) {
                                originRow = _indexStartPeriod.row;
                                NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:originRow + i inSection:indexPath_.section];
                                CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                                [cellPrev setRolCell:kRolElementDefault];
                            }else if (originSection + j == indexPath_.section){
                                originRow = 0;
                                if (originRow + i < indexPath_.row) {
                                    NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:originRow + i inSection:indexPath_.section];
                                    CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                                    [cellPrev setRolCell:kRolElementDefault];
                                }else{
                                    NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:originRow + i inSection:originSection + j];
                                    CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                                    [cellPrev setRolCell:kRolElementPeriodCentral];
                                }
                            }else{
                                NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:originRow + i inSection:originSection + j];
                                CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                                [cellPrev setRolCell:kRolElementDefault];
                            }
                        }else if (indexPath_.section < _indexStartPeriod.section){
                            originSection = indexPath_.section;
                            if (originSection + j == indexPath_.section) {
                                originRow = indexPath_.row;
                                NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:originRow + i inSection:originSection + j];
                                CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                                [cellPrev setRolCell:kRolElementPeriodCentral];
                            }else if (originSection + j == _indexStartPeriod.section){
                                originRow = 0;
                                if (originRow + i < _indexStartPeriod.row) {
                                    NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:originRow + i inSection:_indexStartPeriod.section];
                                    CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                                    [cellPrev setRolCell:kRolElementDefault];
                                }else{
                                    NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:originRow + i inSection:originSection + j];
                                    CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                                    [cellPrev setRolCell:kRolElementPeriodCentral];
                                }
                            }else{
                                NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:originRow + i inSection:originSection + j];
                                CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                                [cellPrev setRolCell:kRolElementDefault];
                            }
                        }
                    }
                }
            }
            _indexStartPeriod = indexPath_;
            _indexHighlightedPrev = _indexStartPeriod;
            [cell setRolCell:kRolElementPeriodStarter];
            if ([self.delegate respondsToSelector:@selector(onTapElementFromCalendar:)]) {
                [_delegate performSelector:@selector(onTapElementFromCalendar:) withObject:[self stringForTextFieldFromDate: dateSelected]];
                [self configureMonthButton:YES];
            }
            if (_indexStartPeriod) {
                CRPCalendarItemCell *cellStart = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:_indexStartPeriod];
                [cellStart setRolCell:kRolElementPeriodStarterDisabled];
                if (_indexEndPeriod) {
                    CRPCalendarItemCell *cellEnd = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:_indexEndPeriod];
                    [cellEnd setRolCell:kRolElementPeriodEnder];
                }
            }
            _onStartPeriod = YES;
            [collectionView reloadSections:[NSIndexSet indexSetWithIndex:_indexStartPeriod.section]];
        }else if (self.accessMode == CRPCalendarTillTodayPeriodMode && _onStartPeriod){
            
            NSIndexPath *prevIndexTap = nil;
            NSIndexPath *nextIndexTap = nil;
            [cell setRolCell:kRolElementPeriodEnder];
            if([_indexHighlightedPrev compare:nextIndexTap] < 0){
                prevIndexTap = _indexHighlightedPrev;
                NSIndexPath *indexPathConverted = [self indexPathConversionDependsOnScrollOrientation:indexPath inverse:YES];
                nextIndexTap = indexPathConverted;
                
                for (long j = prevIndexTap.section; j < nextIndexTap.section+1L ; j++) {
                    for (long i = prevIndexTap.row; i < nextIndexTap.row+1L; i++) {
                        CRPCalendarItemCell *cell = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:j]];
                        [cell setHighlighted:!cell.highlighted];
                    }
                }
            }else{
                NSIndexPath *indexPathConverted = [self indexPathConversionDependsOnScrollOrientation:indexPath inverse:YES];
                prevIndexTap = indexPathConverted;
                nextIndexTap = _indexHighlightedPrev;
                
                for (long j = prevIndexTap.section; j < nextIndexTap.section+1L; j++) {
                    for (long i = prevIndexTap.row; i < nextIndexTap.row+1L; i++) {
                        CRPCalendarItemCell *cell = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:j]];
                        [cell setHighlighted:!cell.highlighted];
                    }
                }
            }
            
            if (_indexEndPeriod) {
                for(long i=0; i< labs(indexPath.row - _indexEndPeriod.row) + 1L; i++){
                    long origin = 0L;
                    if (indexPath.row < _indexEndPeriod.row) {
                        origin = indexPath.row;
                    }else{
                        origin = _indexEndPeriod.row;
                    }
                    NSIndexPath *indexIterator = [NSIndexPath indexPathForRow:origin + i inSection:_indexEndPeriod.section];
                    CRPCalendarItemCell *cellPrev = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:indexIterator];
                    [cellPrev setRolCell:kRolElementDefault];
                }
            }
            
            _indexHighlightedPrev = indexPath;
            
            _indexEndPeriod = indexPath;
            [self completePeriodBetween:_indexStartPeriod and:_indexEndPeriod];
            if ([self.delegate respondsToSelector:@selector(onTapElementFromCalendar:)]) {
                [_delegate performSelector:@selector(onTapElementFromCalendar:) withObject:[self stringForTextFieldFromDate:dateSelected]];
                [self configureMonthButton:YES];
            }
            if (_indexEndPeriod) {
                CRPCalendarItemCell *cellEnd = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:_indexEndPeriod];
                [cellEnd setRolCell:kRolElementPeriodEnderDisabled];
                if (_indexStartPeriod) {
                    CRPCalendarItemCell *cellStart = (CRPCalendarItemCell *)[collectionView cellForItemAtIndexPath:_indexStartPeriod];
                    [cellStart setRolCell:kRolElementPeriodStarter];
                }
            }
            _onStartPeriod = NO;
            [collectionView reloadSections:[NSIndexSet indexSetWithIndex:_indexEndPeriod.section]];
        }else{
            if (isIndexUpLimit && isIndexDownLimit) {
                self.dateSelected = dateSelected;
                _indexPathDateSelected = indexPath_;
            }else{
                return;
            }
            if ([self.delegate respondsToSelector:@selector(onTapElementFromCalendar:)]) {
                id response = nil;
                response = self.dateSelected;
                
                [_delegate performSelector:@selector(onTapElementFromCalendar:) withObject:[self stringForTextFieldFromDate:response]];
                if (!IS_IPAD) {
                    [self configureMonthButton:YES];
                }
                _indexPathCurrent = indexPath_;
            }
        }
    }
}

#pragma mark - IBActions

- (IBAction)onTapNextMonth:(id)sender{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorianCalendar components:NSMonthCalendarUnit fromDate:[NSDate date]];
    long month = [comps month];
    comps = [gregorianCalendar components:NSYearCalendarUnit fromDate:[NSDate date]];
    long yearToday = [comps year];
    
    long year = floor(_indexPathCurrent.section/12);
    long yearSelected = yearToday - (kNumYearsAtComponent/2) +year;
    
    if (!((self.accessMode == CRPCalendarTillTodayMode || self.accessMode == CRPCalendarTillTodayPeriodMode || self.accessMode == CRPCalendarTillTodayModeWithButton)  && month-1 == _indexPathCurrent.section%12 && yearToday == yearSelected)) {
        _indexPathCurrent = [NSIndexPath indexPathForRow:0 inSection:_indexPathCurrent.section+1];
        NSIndexPath *indexPathRealForCollectionViewOrientation = [self indexPathConversionDependsOnScrollOrientation:_indexPathCurrent inverse:NO];
        UICollectionViewScrollPosition position = UICollectionViewScrollPositionNone;
        if ([((UICollectionViewFlowLayout *)[self.collectionCalendarView collectionViewLayout]) scrollDirection] == UICollectionViewScrollDirectionHorizontal) {
            position = UICollectionViewScrollPositionLeft;
        }else{
            position = UICollectionViewScrollPositionTop;
        }
        [self.collectionCalendarView scrollToItemAtIndexPath:indexPathRealForCollectionViewOrientation atScrollPosition:position animated:NO];
        
        _dateIteratorFirstDayMonth = [self dateFromSelection:[NSIndexPath indexPathForRow:_indexPathCurrent.row inSection:_indexPathCurrent.section]];
        NSString * strTitle = [self fromDateToLiteralTitle:_dateIteratorFirstDayMonth];
        [self.lblMonthYear setText:strTitle];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *comps = [gregorianCalendar components:NSMonthCalendarUnit fromDate:[NSDate date]];
        long month = [comps month];
        
        if ((self.accessMode == CRPCalendarTillTodayMode || self.accessMode == CRPCalendarTillTodayPeriodMode || self.accessMode == CRPCalendarTillTodayModeWithButton) && month-1 != _indexPathCurrent.section%12) {
            [self configureFromTodayAccessMode:YES];
        }
    }
    
    if ((self.accessMode == CRPCalendarTillTodayMode || self.accessMode == CRPCalendarTillTodayPeriodMode || self.accessMode == CRPCalendarTillTodayModeWithButton) && month-1 == _indexPathCurrent.section%12 && yearToday == yearSelected) {
        [self configureTillTodayAccessMode:NO];
    }else{
        [self configureFromTodayAccessMode:YES];
    }
    
    if ([self.delegate respondsToSelector:@selector(onTapChangeMonth:)]) {
        [self.delegate performSelector:@selector(onTapChangeMonth:) withObject:@{kYear:[NSNumber numberWithLong:yearSelected], kMonth:[NSNumber numberWithInt:_indexPathCurrent.section%12]}];
    }
    
    if (self.accessMode == CRPCalendarTillTodayPeriodMode) {
        [self completePeriodBetween:_indexStartPeriod and:_indexEndPeriod];
    }
}

- (IBAction)onTapPreviousMonth:(id)sender{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorianCalendar components:NSMonthCalendarUnit fromDate:[NSDate date]];
    long month = [comps month];
    comps = [gregorianCalendar components:NSYearCalendarUnit fromDate:[NSDate date]];
    long yearToday = [comps year];
    
    long year = floor(_indexPathCurrent.section/12);
    long yearSelected = yearToday - (kNumYearsAtComponent/2) +year;
    
    if (!((self.accessMode == CRPCalendarFromTodayMode || self.accessMode == CRPCalendarFromTodayModeWithButton) && month-1 == _indexPathCurrent.section%12 && yearToday == yearSelected)) {
        _indexPathCurrent = [NSIndexPath indexPathForRow:0 inSection:_indexPathCurrent.section-1];
        NSIndexPath *indexPathRealForCollectionViewOrientation = [self indexPathConversionDependsOnScrollOrientation:_indexPathCurrent inverse:NO];
        UICollectionViewScrollPosition position = UICollectionViewScrollPositionNone;
        if ([((UICollectionViewFlowLayout *)[self.collectionCalendarView collectionViewLayout]) scrollDirection] == UICollectionViewScrollDirectionHorizontal) {
            position = UICollectionViewScrollPositionLeft;
        }else{
            position = UICollectionViewScrollPositionTop;
        }
        [self.collectionCalendarView scrollToItemAtIndexPath:indexPathRealForCollectionViewOrientation atScrollPosition:position animated:NO];
        
        _dateIteratorFirstDayMonth = [self dateFromSelection:[NSIndexPath indexPathForRow:_indexPathCurrent.row inSection:_indexPathCurrent.section]];
        
        NSString * strTitle = [self fromDateToLiteralTitle:_dateIteratorFirstDayMonth];
        [self.lblMonthYear setText:strTitle];
        
        if ((self.accessMode == CRPCalendarTillTodayMode || self.accessMode == CRPCalendarTillTodayPeriodMode || self.accessMode == CRPCalendarTillTodayModeWithButton)&& month-1 != _indexPathCurrent.section%12) {
            [self configureTillTodayAccessMode:YES];
            if (self.accessMode == CRPCalendarTillTodayPeriodMode) {
                [self completePeriodBetween:_indexStartPeriod and:_indexEndPeriod];
            }
        }
    }
    
    if ((self.accessMode == CRPCalendarFromTodayMode || self.accessMode == CRPCalendarFromTodayModeWithButton) && month-1 == _indexPathCurrent.section%12 && yearToday == yearSelected) {
        [self configureFromTodayAccessMode:NO];
    }else{
        [self configureFromTodayAccessMode:YES];
    }
    
    if ([self.delegate respondsToSelector:@selector(onTapChangeMonth:)]) {
        [self.delegate performSelector:@selector(onTapChangeMonth:) withObject:@{kYear:[NSNumber numberWithLong:yearSelected], kMonth:[NSNumber numberWithLong:_indexPathCurrent.section%12]}];
    }
}

- (IBAction)onTapCurrentMonth:(id)sender{
    if ([self.delegate respondsToSelector:@selector(onTapMonthButton:)]) {
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *comps = [gregorianCalendar components:NSYearCalendarUnit fromDate:[NSDate date]];
        long yearToday = [comps year];
        long year = floor(_indexPathCurrent.section/12);
        long yearSelected = yearToday - (kNumYearsAtComponent/2) +year;
        
        [self.delegate performSelector:@selector(onTapMonthButton:) withObject:@{kYear:[NSNumber numberWithLong:yearSelected], kMonth:[NSNumber numberWithLong:_indexPathCurrent.section%12]}];
        [self configureMonthButton:NO];
        
        [self.collectionCalendarView deselectItemAtIndexPath:_indexPathCurrent animated:YES];
    }
}

#pragma mark - setter & getter

- (void)setDateSelected:(NSDate *)dateSelected{
    
    _dateSelected = dateSelected;
    
    NSString * strTitle = [self fromDateToLiteralTitle:_dateSelected];
    
    [self.lblMonthYear setText:strTitle];
}

- (void)setAccessMode:(CRPCalendarAccessMode)accessMode{
    _accessMode = accessMode;
    if (_accessMode == CRPCalendarTillTodayMode || _accessMode == CRPCalendarTillTodayPeriodMode || _accessMode == CRPCalendarTillTodayModeWithButton) {
        [self configureTillTodayAccessMode:NO];
    }
    if (_accessMode == CRPCalendarFromTodayMode || _accessMode == CRPCalendarFromTodayModeWithButton) {
        [self configureFromTodayAccessMode:NO];
    }
    [self configureMonthButton:NO];
}

- (void)setDelegate:(id<CRPCalendarDelegate>)delegate{
    _delegate = delegate;
    if ([_delegate respondsToSelector:@selector(onTapElementFromCalendar:)] && self.accessMode != CRPCalendarTillTodayPeriodMode) {
        [_delegate performSelector:@selector(onTapElementFromCalendar:) withObject:[self stringForTextFieldFromDate:self.dateSelected]];
        [self configureMonthButton:YES];
    }
}

#pragma mark - auxiliary methods

- (void)selectCurrentElement{
    NSIndexPath *indexSelected = [self indexPathConversionDependsOnScrollOrientation:[self indexFromDate:_dateSelected] inverse:NO];
    [self.collectionCalendarView selectItemAtIndexPath:indexSelected animated:NO scrollPosition:UICollectionViewScrollPositionNone];
}

-(void)resetCalendar{
    if ([_delegate respondsToSelector:@selector(onTapElementFromCalendar:)] && self.accessMode != CRPCalendarTillTodayPeriodMode) {
        [_delegate performSelector:@selector(onTapElementFromCalendar:) withObject:[self stringForTextFieldFromDate:_dateSelected]];
        [self configureMonthButton:YES];
    }else{
        _indexHighlightedPrev = nil;
        _indexEndPeriod = nil;
        _indexStartPeriod = nil;
        _onStartPeriod = NO;
        [_collectionCalendarView reloadData];
    }
}

-(void)resetCalendarGoingToday{
    _dateSelected = [NSDate date];
    _indexPathDateSelected = nil;
    [self setAccessMode:self.accessMode];
    [self resetCalendarGoingSelectedDate];
}

-(void)resetCalendarGoingSelectedDate{
    [self resetCalendar];
    [self setDateSelected:_dateSelected];
    NSIndexPath *indexPathToday = [self indexFromDate:_dateSelected];
    _indexPathCurrent = [NSIndexPath indexPathForRow:0L inSection:indexPathToday.section];
    
    [self.collectionCalendarView scrollToItemAtIndexPath:_indexPathCurrent  atScrollPosition:UICollectionViewScrollPositionTop animated:NO];

    [self configureFromTodayAccessMode:YES];
    
}

- (NSString *)fromDateToLiteralTitle:(NSDate *)dateSelected{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorianCalendar components:NSYearCalendarUnit fromDate:dateSelected];
    long yearDate = [comps year];
    comps = [gregorianCalendar components:NSMonthCalendarUnit fromDate:dateSelected];
    long monthDate = [comps month];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
    NSString *monthName = [[dateFormatter monthSymbols] objectAtIndex:(monthDate-1)];
    return [NSString stringWithFormat:@"%@ %ld", monthName, yearDate];
}

- (NSDate *)dateFromSelection:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = [self configurationParamsForIndex:indexPath];
    long daySelected = indexPath.row + 1L - [[dict objectForKey:kInitialWeekDay] integerValue];
    if (daySelected <= 0) {
        daySelected = 1;
    }
    int monthSelected = (indexPath.section%12)+1;
    
    NSDate *today = [NSDate date]; //Get a date object for today's date
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorianCalendar components:NSYearCalendarUnit fromDate:today];
    long yearToday = [comps year];
    long year = floor(indexPath.section/12);
    long yearSelected = yearToday - (kNumYearsAtComponent/2) +year;
    
    comps = [[NSDateComponents alloc] init];
    [comps setDay:daySelected];
    [comps setMonth:monthSelected];
    [comps setYear:yearSelected];
    NSDate *dateSelected =[gregorianCalendar dateFromComponents:comps];
    return dateSelected;
}

- (NSIndexPath *)indexFromDate:(NSDate *)dateSelected {
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorianCalendar components:NSDayCalendarUnit fromDate:dateSelected];
    long day = [comps day];
    comps = [gregorianCalendar components:NSYearCalendarUnit fromDate:dateSelected];
    long year = [comps year];
    comps = [gregorianCalendar components:NSMonthCalendarUnit fromDate:dateSelected];
    long month = [comps month];
    
    NSDate *today = [NSDate date]; //Get a date object for today's date
    comps = [gregorianCalendar components:NSYearCalendarUnit fromDate:today];
    long yearToday = [comps year];
    
    long section = ((year - yearToday + (kNumYearsAtComponent/2))*12L)+month-1L;
    
    long row = (day-1L) + [[self startWeekdayForMonthOfDate:dateSelected] integerValue];
    
    return [NSIndexPath indexPathForRow:row inSection:section];
}

- (NSNumber *)startWeekdayForMonthOfDate:(NSDate *)date{
    NSNumber *numOffset = nil;
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorianCalendar components:NSMonthCalendarUnit fromDate:date];
    long month = [comps month];
    comps = [gregorianCalendar components:NSYearCalendarUnit fromDate:date];
    long year = [comps year];
    
    NSDateComponents *dateComp = [[NSDateComponents alloc] init];
    [dateComp setDay:1];
    [dateComp setMonth:month];
    [dateComp setYear:year];
    NSDate *dateFirstDayOfMonht =[gregorianCalendar dateFromComponents:dateComp];
    
    comps = [gregorianCalendar components:NSWeekdayCalendarUnit fromDate:dateFirstDayOfMonht];

    if ([comps weekday] == 1) {
        numOffset = @(7 + [comps weekday]-2);
    }else{
        numOffset = @([comps weekday]-2);
    }
    return numOffset;
}

- (NSNumber *)numberOfDaysAtMonthForDate:(NSDate *)date{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSRange days = [gregorianCalendar rangeOfUnit:NSDayCalendarUnit
                                           inUnit:NSMonthCalendarUnit
                                          forDate:date];
    return @(days.length);
}

- (NSIndexPath *)indePathFromPoint:(CGPoint)point{
    
    int section = floor (point.y/(kHeightCalendarCell*kNumElementsAtCalendarColumn));
    
    int yInSection = floor ((point.y - (section*kHeightCalendarCell*kNumElementsAtCalendarColumn))/kHeightCalendarCell);
    int xInSection = floor (point.x/kWidthCalendarCell);
    
    int row = (yInSection * kNumElementsAtCalendarRow) + xInSection;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    return indexPath;
}

- (void)configureDefaultAccessMode{
    self.dateSelected = [NSDate date];
    NSIndexPath *indexSelected = [self indexPathConversionDependsOnScrollOrientation:[self indexFromDate:_dateSelected] inverse:NO];
    _indexPathCurrent = [NSIndexPath indexPathForRow:0 inSection:indexSelected.section];
    if ([self.delegate respondsToSelector:@selector(onTapChangeMonth:)]) {
        [self.delegate performSelector:@selector(onTapChangeMonth:) withObject:_indexPathCurrent];
    }
    NSIndexPath *indexPathRealForCollectionViewOrientation = [self indexPathConversionDependsOnScrollOrientation:_indexPathCurrent inverse:NO];
    UICollectionViewScrollPosition position = UICollectionViewScrollPositionNone;
    if ([((UICollectionViewFlowLayout *)[self.collectionCalendarView collectionViewLayout]) scrollDirection] == UICollectionViewScrollDirectionHorizontal) {
        position = UICollectionViewScrollPositionLeft;
    }else{
        position = UICollectionViewScrollPositionTop;
    }
    [self.collectionCalendarView scrollToItemAtIndexPath:indexPathRealForCollectionViewOrientation atScrollPosition:position animated:NO];
    [self.collectionCalendarView selectItemAtIndexPath:indexSelected animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    
    _dateIteratorFirstDayMonth = [self dateFromSelection:_indexPathCurrent];
}

- (void)configureFromTodayAccessMode:(BOOL)enable{
    //Disable previous month Selector
//    [self.btnPrevious setEnabled:enable];
    [self.btnPrevious setHidden:!enable];
    if (enable || IS_IPAD) {
        [self.containerLeft setAlpha:1.0];
    }else{
        [self.containerLeft setAlpha:0.0];
    }
}

- (void)configureTillTodayAccessMode:(BOOL)enable{
    //Disable next month Selector
//    [self.btnNext setEnabled:enable];
    [self.btnNext setHidden:!enable];
    if (enable || IS_IPAD) {
        [self.containerRight setAlpha:1.0];
    }else{
        [self.containerRight setAlpha:0.0];
    }
    NSIndexPath *indexSelected = [self indexPathConversionDependsOnScrollOrientation:[self indexFromDate:self.dateSelected] inverse:NO];
    [self.collectionCalendarView deselectItemAtIndexPath:indexSelected animated:NO];
}

- (NSDictionary *)configurationParamsForIndex:(NSIndexPath *)indexPath {
    NSDate *today = [NSDate date]; //Get a date object for today's date
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorianCalendar components:NSYearCalendarUnit fromDate:today];
    long yearToday = [comps year];
    
    long month = (indexPath.section%12)+1L;
    long year = floor(indexPath.section/12);
    
    comps = [[NSDateComponents alloc] init];
    [comps setDay:1];
    [comps setMonth:month];
    [comps setYear:yearToday - (kNumYearsAtComponent/2) +year];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *fechaMesActual = [gregorian dateFromComponents:comps];
    
    comps = [gregorianCalendar components:NSWeekdayCalendarUnit fromDate:fechaMesActual];
    NSRange days = [gregorianCalendar rangeOfUnit:NSDayCalendarUnit
                                           inUnit:NSMonthCalendarUnit
                                          forDate:fechaMesActual];
    NSNumber *numWeekDay = nil;
    if ([comps weekday] == 1) {
        numWeekDay = @(7 + [comps weekday]-2);
    }else{
        numWeekDay = @([comps weekday]-2);
    }
    
    NSDictionary *dictCalendarItem = @{kInitialWeekDay:numWeekDay, kNumDaysMonth:@(days.length)};
    return dictCalendarItem;
}

- (NSIndexPath *)indexPathConversionDependsOnScrollOrientation:(NSIndexPath *)indexPathOriginal inverse:(BOOL)inverse{
    NSIndexPath *indexPathConverted = nil;
    if (inverse){
        if ([((UICollectionViewFlowLayout *)[self.collectionCalendarView collectionViewLayout]) scrollDirection] == UICollectionViewScrollDirectionHorizontal){
            indexPathConverted = [NSIndexPath indexPathForRow:indexPathOriginal.row %7 + (indexPathOriginal.row/7 * 6) inSection:indexPathOriginal.section];
        }else{
            indexPathConverted = indexPathOriginal;
        }
    }else{
        if ([((UICollectionViewFlowLayout *)[self.collectionCalendarView collectionViewLayout]) scrollDirection] == UICollectionViewScrollDirectionHorizontal){
            indexPathConverted = [NSIndexPath indexPathForRow:indexPathOriginal.row/6 + (indexPathOriginal.row%6 * 7) inSection:indexPathOriginal.section];
        }else{
            indexPathConverted = indexPathOriginal;
        }
    }
    return indexPathConverted;
}

- (void)setArrayEventsData:(NSArray *)arrayEventsData{
    _arrayEventsData = arrayEventsData;
    for (NSDictionary *dictEvent in arrayEventsData) {
        NSInteger month = [[dictEvent objectForKey:kMonth] integerValue]+1;
        NSInteger year = [[dictEvent objectForKey:kYear] integerValue];
        for (NSDictionary *dictPayment in [dictEvent objectForKey:kPayments]) {
            NSInteger day = [[dictPayment objectForKey:kDay] integerValue];
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            
            NSDateComponents *dateComp = [[NSDateComponents alloc] init];
            [dateComp setDay:day];
            [dateComp setMonth:month];
            [dateComp setYear:year];
            NSDate *dateEvent =[gregorianCalendar dateFromComponents:dateComp];
            
            NSIndexPath *indexEvent = [self indexFromDate:dateEvent];
            
            CRPCalendarItemCell *cell = (CRPCalendarItemCell *)[self collectionView:self.collectionCalendarView cellForItemAtIndexPath:indexEvent];
            [cell setRolCell:kRolElementEventsScheduled];
            
            if (!_eventsScheduled) {
                _eventsScheduled = [[NSMutableSet alloc] init];
            }
            [_eventsScheduled addObject:indexEvent];
        }
    }
}

- (void)configureMonthButton:(BOOL)mustShow{
    if (self.accessMode == CRPCalendarFromTodayModeWithButton || self.accessMode == CRPCalendarTillTodayModeWithButton) {
        [self.lblMonthYear setHidden:mustShow];
    }
}

//In: Date
//Out: String from date in with format "dd MMM yyyy"
- (NSString *)stringForTextFieldFromDate:(NSDate*)dateIn{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
    if (IS_IPAD) {
        [formatter setDateFormat:@"dd MMMM yyyy"];
    }else{
        [formatter setDateFormat:@"dd MMM yyyy"];
    }
    return [formatter stringFromDate:dateIn];
}

@end
