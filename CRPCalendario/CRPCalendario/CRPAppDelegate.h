//
//  CRPAppDelegate.h
//  CRPCalendario
//
//  Created by CARLOS RAUL PEREZ MORENO on 16/07/14.
//
//

#import <UIKit/UIKit.h>

@interface CRPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
