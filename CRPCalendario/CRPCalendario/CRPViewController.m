//
//  CRPViewController.m
//  CRPCalendario
//
//  Created by CARLOS RAUL PEREZ MORENO on 16/07/14.
//
//

#import "CRPViewController.h"
#import "CRPCalendarComponent.h"

@interface CRPViewController () <CRPCalendarDelegate>

@property (weak, nonatomic) IBOutlet CRPCalendarComponent *calendarComponent;
@property (weak, nonatomic) IBOutlet UILabel *lblDateSelected;

@end

@implementation CRPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _calendarComponent.accessMode = CRPCalendarTillTodayMode;
    [_calendarComponent setDelegate:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onTapElementFromCalendar:(id)selectedDate{
    if ([selectedDate isKindOfClass:[NSString class]]) {
        [self.lblDateSelected setText:selectedDate];
    }
}

@end
